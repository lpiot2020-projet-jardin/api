package com.example.restserviceapipj.ressources;


import java.util.List;
import java.util.logging.Logger;

import com.example.restserviceapipj.decorators.auth.AuthUser;
import com.example.restserviceapipj.entities.IoT;
import com.example.restserviceapipj.exceptions.NoPermissionsEspaceException;
import com.example.restserviceapipj.ressources.input.CreateIoTInput;
import com.example.restserviceapipj.ressources.input.DeleteIoTInput;
import com.example.restserviceapipj.ressources.input.RefreshIoTTokenInput;
import com.example.restserviceapipj.ressources.input.UpdateIoTInput;
import com.example.restserviceapipj.services.EspacesService;
import com.example.restserviceapipj.services.IoTService;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.http.HTTPException;

@RestController
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class IoTRessource {

    @Autowired
    IoTService ioTService;

    @Autowired
    EspacesService espacesService;

    private static final Logger LOG = Logger.getLogger("IotRessource");


    @GetMapping("/iot")
    @ResponseBody
    public List<IoT> getMyIoTs(@AuthUser String sub) {
            return this.ioTService.getAllIoT(sub);

    }

    @GetMapping("/iot/refresh-token")
    @ResponseBody
    public Object refreshMyIoTToken(@RequestBody RefreshIoTTokenInput refreshIoTTokenInput, @AuthUser String sub) {
        if (!espacesService.getAllEspaces(sub).stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch(ioT -> ioT.getIdToken().equals(refreshIoTTokenInput.getIdToken())))) {
            return Response.status(403, "Cet IoT ne vous appartient pas").build();
        }
        return Response.ok(this.ioTService.refreshTokenIoT(refreshIoTTokenInput.getIdToken())).build().getEntity();
    }

    @PostMapping("/iot")
    @ResponseBody
    public Object addIoT(@RequestBody CreateIoTInput createIoTInput, @AuthUser String sub) {
        if (!espacesService.getAllEspaces(sub).stream().anyMatch(espaces -> espaces.getId() == createIoTInput.getEspaceId())) {
            return Response.status(403, "Cet espace ne vous appartient pas").build();
        }
            return Response.ok(this.ioTService.addIoT(createIoTInput.getName(), createIoTInput.getDescription(), createIoTInput.getEspaceId())).build().getEntity();

    }

    @PutMapping("/iot")
    @ResponseBody
    public Object updateIoT(@RequestBody UpdateIoTInput updateIoTInput, @AuthUser String sub) {
        String name = updateIoTInput.getName().isPresent() ? updateIoTInput.getName().get() : null;
        String description = updateIoTInput.getDescription().isPresent() ? updateIoTInput.getDescription().get() : null;
        if (!espacesService.getAllEspaces(sub).stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch(ioT -> ioT.getIdToken().equals(updateIoTInput.getIdToken())))) {
            return Response.status(403, "Cet IoT ne vous appartient pas").build();
        }
        return Response.ok(this.ioTService.updateIoT(updateIoTInput.getIdToken(), name, description)).build().getEntity();
    }

    @DeleteMapping("/iot")
    @ResponseBody
    public Object deleteIoT(@RequestBody DeleteIoTInput deleteIoTInput, @AuthUser String sub) {
        if (!espacesService.getAllEspaces(sub).stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch(ioT -> ioT.getIdToken().equals(deleteIoTInput.getIdToken())))) {
            return Response.status(403, "Cet IoT ne vous appartient pas").build();
        }
        return Response.ok(this.ioTService.deleteIoT(deleteIoTInput.getIdToken())).build().getEntity();
    }


}
