package com.example.restserviceapipj.ressources;


import com.example.restserviceapipj.configuration.ws.WebsocketEnum;
import com.example.restserviceapipj.configuration.ws.WebsocketMessage;
import com.example.restserviceapipj.decorators.auth.AuthUser;
import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.entities.Variable;
import com.example.restserviceapipj.ressources.input.*;
import com.example.restserviceapipj.services.UserService;
import com.example.restserviceapipj.services.UtilsService;
import com.example.restserviceapipj.services.VariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestScoped
public class VariableRessource {

    @Autowired
    VariableService variableService;

    @Autowired
    UtilsService utilsService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    UserService userService;

    @GetMapping("/variable")
    @ResponseBody
    public Object getMyVariables(@RequestBody GetMyVariableOfIoTInput getMyVariableOfIoTInput, @AuthUser String sub) {
        User user = this.userService.findOneBySub(sub);
        if(!user.getEspaces().stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch( ioT -> ioT.getIdToken().equals(getMyVariableOfIoTInput.getIdToken())))) {
            return Response.status(403, "Cet IoT ne vous appartient pas").build();
        }
        return this.variableService.getAllVariableByIoTIdToken(getMyVariableOfIoTInput.getIdToken(), sub);
    }

    @PostMapping("/variable")
    @ResponseBody
    public Object addVariable(@RequestBody CreateVariableInput createVariableInput, @AuthUser String sub) {
        User user = this.userService.findOneBySub(sub);
        if(!user.getEspaces().stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch( ioT -> ioT.getId() == createVariableInput.getIotId()))) {
            return Response.status(403, "Cet IoT ne vous appartient pas").build();
        }
        return Response.ok(this.variableService.addVariable(createVariableInput.getLabel(), createVariableInput.getIotId())).build().getEntity();

    }

    @PostMapping("/variable/value/update")
    @ResponseBody
    public Object updateVariableValue(@RequestBody UpdateVariableValueInput updateVariableValueInput) {
        this.variableService.updateVariableValue(updateVariableValueInput.getIdVariable(), updateVariableValueInput.getIdTokenIot(), updateVariableValueInput.getTokenIot(), updateVariableValueInput.getValue());
        Variable variable = this.variableService.findByIdVariable(updateVariableValueInput.getIdVariable());
        if(variable.getSeuil() != null && variable.getValue() > variable.getSeuil()) {
            try {
                this.utilsService.sendUpdates(variable.getLabel(), updateVariableValueInput.getValue());
                WebsocketMessage wsm = new WebsocketMessage(
                        WebsocketEnum.UPDATE.toString(),
                        updateVariableValueInput.getValue().toString(),
                        new SimpleDateFormat("HH:mm").format(new Date()));
                simpMessagingTemplate.convertAndSend("/ws", updateVariableValueInput.getValue().toString());
            } catch (Exception e) {
                throw new ServerErrorException("Erreur", 500);
            }
        }
        return variable;
    }

    @PutMapping("/variable")
    @ResponseBody
    public Object updateVariable(@RequestBody UpdateVariableInput updateVariableInput, @AuthUser String sub) {
        String label = updateVariableInput.getLabel().isPresent() ? updateVariableInput.getLabel().get() : null;
        Float seuil = updateVariableInput.getSeuil().isPresent() ? updateVariableInput.getSeuil().get() : null;
        User user = this.userService.findOneBySub(sub);

        if(!user.getEspaces().stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch(ioT -> ioT.getVariables().stream().anyMatch(variable -> variable.getIdVariable().equals(updateVariableInput.getIdVariable()))))) {
            return Response.status(403, "Cette variable ne vous appartient pas").build();
        }
        return Response.ok(this.variableService.updateVariable(updateVariableInput.getIdVariable(), label, seuil)).build().getEntity();

    }

    @DeleteMapping("/variable")
    @ResponseBody
    public Object deleteVariable(@RequestBody DeleteVariableInput deleteVariableInput, @AuthUser String sub) {
        User user = this.userService.findOneBySub(sub);
        if(!user.getEspaces().stream().anyMatch(espaces -> espaces.getIots().stream().anyMatch( ioT -> ioT.getVariables().stream().anyMatch(variable -> variable.getIdVariable().equals(deleteVariableInput.getIdVariable()))))) {
            return Response.status(403, "Cette variable ne vous appartient pas").build();
        }
        return Response.ok(this.variableService.deleteVariable(deleteVariableInput.getIdVariable())).build().getEntity();
    }

}