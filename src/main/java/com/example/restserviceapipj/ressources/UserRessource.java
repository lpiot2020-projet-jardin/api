package com.example.restserviceapipj.ressources;
import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.ressources.input.LoginInput;
import com.example.restserviceapipj.ressources.input.NewUserInput;
import com.example.restserviceapipj.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;

@RestController
public class UserRessource {


    @Autowired
    UserService userService;

    @PostMapping("/login")
    @ResponseBody
    public Object login(@RequestBody LoginInput loginInput) throws Exception {
        return Response.ok(this.userService.loginUser(loginInput)).build().getEntity();
    }

    @PostMapping("/register")
    @ResponseBody
    public Object register(@RequestBody NewUserInput newUserInput) throws Exception {
        return Response.ok(this.userService.registerUser(newUserInput)).build().getEntity();
    }

}
