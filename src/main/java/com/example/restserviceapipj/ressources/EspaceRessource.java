package com.example.restserviceapipj.ressources;


import java.util.List;

import com.example.restserviceapipj.decorators.auth.AuthUser;
import com.example.restserviceapipj.entities.Espaces;
import com.example.restserviceapipj.ressources.input.CreateEspaceInput;
import com.example.restserviceapipj.ressources.input.DeleteEspaceInput;
import com.example.restserviceapipj.ressources.input.UpdateEspaceInput;
import com.example.restserviceapipj.services.EspacesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;

@RestController
@RequestScoped
public class EspaceRessource {

    @Autowired
    EspacesService espacesService;

    @GetMapping("/espace")
    @ResponseBody
    public List<Espaces> getAllMySpaces(@AuthUser String sub) {
        return this.espacesService.getAllEspaces(sub);
    }

    @PostMapping("/espace")
    @ResponseBody
    public Espaces addEspace(@AuthUser String sub, @RequestBody CreateEspaceInput createEspaceInput) {
        return this.espacesService.addEspace(createEspaceInput.getName(), createEspaceInput.getDescription(), sub);
    }

    @PutMapping("/espace")
    @ResponseBody
    public Object updateEspace(@AuthUser String sub, @RequestBody UpdateEspaceInput updateEspaceInput)  {
        String name = updateEspaceInput.getName().isPresent() ? updateEspaceInput.getName().get() : null;
        String description = updateEspaceInput.getDescription().isPresent() ? updateEspaceInput.getDescription().get() : null;
        if (!espacesService.getAllEspaces(sub).stream().anyMatch(espaces -> espaces.getId() == updateEspaceInput.getEspaceId())) {
            return Response.status(403, "Aucun espace vous appartenant avec cet identifiant a été trouvé !");
        }
        return Response.ok(this.espacesService.updateEspace(name, description, updateEspaceInput.getEspaceId())).build().getEntity();
    }

    @DeleteMapping("/espace")
    @ResponseBody
    public Object deleteEspace(@AuthUser String sub, @RequestBody DeleteEspaceInput deleteEspaceInput) {
        if (!espacesService.getAllEspaces(sub).stream().anyMatch(espaces -> espaces.getId() == deleteEspaceInput.getEspaceId())) {
            return Response.status(403, "Aucun espace vous appartenant avec cet identifiant a été trouvé !").build();
        }
        return Response.ok(this.espacesService.deleteEspace(deleteEspaceInput.getEspaceId())).build().getEntity();
    }

}