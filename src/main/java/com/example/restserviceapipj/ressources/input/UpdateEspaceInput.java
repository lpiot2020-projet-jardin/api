package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

import java.util.Optional;

@Getter
public class UpdateEspaceInput {

    Optional<String> name;
    Optional<String> description;
    Long espaceId;
}