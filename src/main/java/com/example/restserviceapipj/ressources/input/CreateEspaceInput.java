package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

@Getter
public class CreateEspaceInput {

    String name;
    String description;

}
