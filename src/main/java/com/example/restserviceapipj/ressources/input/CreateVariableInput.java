package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

@Getter
public class CreateVariableInput {

    String label;
    Long iotId;
}
