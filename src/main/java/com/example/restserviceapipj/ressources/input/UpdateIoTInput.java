package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

import java.util.Optional;

@Getter
public class UpdateIoTInput {

    Optional<String> name;
    Optional<String> description;
    String idToken;
}
