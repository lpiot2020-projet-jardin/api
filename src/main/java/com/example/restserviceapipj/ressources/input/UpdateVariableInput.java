package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

import java.util.Optional;

@Getter
public class UpdateVariableInput {
    String idVariable;
    Optional<String> label;
    Optional<Float> seuil;
}
