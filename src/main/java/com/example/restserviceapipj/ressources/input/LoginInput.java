package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

@Getter
public class LoginInput {

    String username;
    String password;
}
