package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

@Getter
public class NewUserInput {

    String username;
    String firstName;
    String lastName;
    String password;
    String email;
}
