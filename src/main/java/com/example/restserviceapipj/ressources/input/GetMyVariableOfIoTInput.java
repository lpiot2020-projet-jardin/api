package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

@Getter
public class GetMyVariableOfIoTInput {

    String idToken;
}
