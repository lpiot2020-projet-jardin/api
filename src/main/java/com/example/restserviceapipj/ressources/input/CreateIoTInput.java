package com.example.restserviceapipj.ressources.input;

import com.example.restserviceapipj.entities.Espaces;
import lombok.Getter;

@Getter
public class CreateIoTInput {

    String name;
    String description;
    Long espaceId;

}
