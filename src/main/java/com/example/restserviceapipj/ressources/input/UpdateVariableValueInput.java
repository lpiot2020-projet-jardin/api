package com.example.restserviceapipj.ressources.input;

import lombok.Getter;

@Getter
public class UpdateVariableValueInput {
    String idTokenIot;
    String tokenIot;
    String idVariable;
    Float value;
}
