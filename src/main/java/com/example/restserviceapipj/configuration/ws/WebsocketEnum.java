package com.example.restserviceapipj.configuration.ws;

public enum WebsocketEnum {
    UPDATE,
    NEW,
    DELETE
}
