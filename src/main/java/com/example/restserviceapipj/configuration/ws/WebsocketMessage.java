package com.example.restserviceapipj.configuration.ws;


import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
public class WebsocketMessage {

    private String action;
    private String value;
    private String date;

}
