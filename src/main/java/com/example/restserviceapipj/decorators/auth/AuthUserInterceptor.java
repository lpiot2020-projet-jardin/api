package com.example.restserviceapipj.decorators.auth;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.enterprise.context.RequestScoped;
import java.util.logging.Logger;


@Component
@RequestScoped
public class AuthUserInterceptor implements HandlerMethodArgumentResolver {

    private static final Logger LOG = Logger.getLogger("UserInterceptor");

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(AuthUser.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        KeycloakAuthenticationToken kp = (KeycloakAuthenticationToken) nativeWebRequest.getUserPrincipal();
        return kp.getAccount().getKeycloakSecurityContext().getToken().getSubject();

    }


}


