package com.example.restserviceapipj.repositories;

import com.example.restserviceapipj.entities.Espaces;
import com.example.restserviceapipj.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacesRepository extends JpaRepository<Espaces, Long> {

    @Query("SELECT E FROM espaces E WHERE E.user = ?1")
    List<Espaces> findAllByUser(User user);
}