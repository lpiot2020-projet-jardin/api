package com.example.restserviceapipj.repositories;

import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.entities.Variable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariableRepository extends JpaRepository<Variable, Long> {

    @Query("SELECT V FROM variables_iot V WHERE V.idVariable = ?1")
    Variable findOneByIdVariable(String idVariable);

    @Query("SELECT V FROM variables_iot V LEFT JOIN iot I ON V.iot = I.id LEFT JOIN espaces E ON E.id = I.espace WHERE I.idToken = ?1 AND E.user = ?2")
    List<Variable> findAllByIoTIdToken(String idToken, User user);
}