package com.example.restserviceapipj.repositories;

import com.example.restserviceapipj.entities.IoT;
import com.example.restserviceapipj.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IoTRepository extends JpaRepository<IoT, Long> {

    @Query("SELECT I FROM iot I LEFT JOIN espaces E ON E.id = I.espace WHERE E.user = ?1")
    List<IoT> findAllByUser(User user);

    @Query("SELECT I FROM iot I WHERE I.idToken = ?1")
    IoT findByIdToken(String idToken);

    @Query("SELECT I FROM iot I WHERE I.idToken = ?1 AND I.token = ?2")
    IoT findByIdTokenAndToken(String idToken, String token);
}
