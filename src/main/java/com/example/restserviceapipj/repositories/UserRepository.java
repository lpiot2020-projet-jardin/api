package com.example.restserviceapipj.repositories;

import com.example.restserviceapipj.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    @Query("SELECT U FROM user U WHERE U.uuid = ?1")
    User findOneBySub(String sub);
}