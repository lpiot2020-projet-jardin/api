package com.example.restserviceapipj.strategies;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.UUID;

/**
 * Class used to generate a random id for iot string of 24 chars
 * @author David BEN KHEMIS
 */
public class IdGenerator {

    public static String generateNewId() {
        return UUID.randomUUID().toString().replace("-", "").substring(0, 23);
    }
}
