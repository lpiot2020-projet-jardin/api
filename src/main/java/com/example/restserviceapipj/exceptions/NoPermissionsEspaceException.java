package com.example.restserviceapipj.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Cet espace ne vous appartient pas")

public class NoPermissionsEspaceException extends RuntimeException {

}
