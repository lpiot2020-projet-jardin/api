package com.example.restserviceapipj.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;


@Getter
@Setter
@Entity(name="iot")
@JsonIgnoreProperties(value = {"espace"})

public class IoT {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;


    private String idToken;


    private String token;

    private String description;

    @ManyToOne
    @JoinColumn(name="espace_id", nullable = false)
    private Espaces espace;

    @OneToMany(mappedBy = "iot")
    private Set<Variable> variables;

}