package com.example.restserviceapipj.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity(name="variables_iot")
@JsonIgnoreProperties(value = {"iot"})
public class Variable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "label")
    private String label;

    @Column(name = "id_variable")
    private String idVariable;

    @Column(name = "value")
    private Float value;

    @ManyToOne
    @JoinColumn(name = "iot_id_token", nullable = false)
    private IoT iot;

    @Column(name = "seuil")
    private Float seuil;

}
