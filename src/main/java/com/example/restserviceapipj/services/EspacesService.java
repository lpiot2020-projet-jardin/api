package com.example.restserviceapipj.services;

import com.example.restserviceapipj.entities.Espaces;
import com.example.restserviceapipj.entities.IoT;
import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.entities.Variable;
import com.example.restserviceapipj.repositories.EspacesRepository;
import com.example.restserviceapipj.repositories.IoTRepository;
import com.example.restserviceapipj.repositories.UserRepository;
import com.example.restserviceapipj.repositories.VariableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;

import java.util.List;
import java.util.logging.Logger;

@Service
public class EspacesService {
    private static final Logger LOG = Logger.getLogger("EspaceService");

    @Autowired
    private EspacesRepository espacesRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IoTRepository ioTRepository;

    @Autowired
    private VariableRepository variableRepository;


    public List<Espaces> getAllEspaces(String sub) {
        User user = this.userRepository.findOneBySub(sub);
        List<Espaces> espaces = this.espacesRepository.findAllByUser(user);
        return espaces;
    }

    public Espaces addEspace(String name, String description, String sub) {
        try {
            Espaces espaces = new Espaces();
            espaces.setName(name);
            espaces.setDescription(description);
            User user = this.userRepository.findOneBySub(sub);
            espaces.setUser(user);
            this.espacesRepository.save(espaces);
            return espaces;
        } catch (Exception e) {
            throw new ServerErrorException("Une erreur est survenue lors de la création de votre espace", 500);
        }
    }

    public Espaces updateEspace(String name, String description, Long espaceId) {
        try {
            Espaces espaces = this.espacesRepository.findById(espaceId).orElseThrow();
            if(name != null) espaces.setName(name);
            if(description != null) espaces.setDescription(description);
            this.espacesRepository.save(espaces);
            return espaces;
        } catch(Exception e) {
            throw new ServerErrorException("Une erreur est survenue lors de la mise à jour de votre espace", 500);
        }
    }

    public boolean deleteEspace(Long espaceId) {
        try {
            Espaces espaces = this.espacesRepository.findById(espaceId).orElseThrow();

            for(IoT i : espaces.getIots()) {
                for(Variable v : i.getVariables())
                    this.variableRepository.delete(v);
                this.ioTRepository.delete(i);
            }

            this.espacesRepository.delete(espaces);
            return true;
        } catch (Exception e) {
            throw new ServerErrorException("Une erreur est survenue lors de la suppression de votre espace", 500);
        }
    }
}