package com.example.restserviceapipj.services;

import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.repositories.UserRepository;
import com.example.restserviceapipj.ressources.input.LoginInput;
import com.example.restserviceapipj.ressources.input.NewUserInput;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

@Service
public class UserService {
    private static final Logger LOG = Logger.getLogger("UserService");

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KeycloakAdminService keycloakAdminService;

    @Value("${login.keycloak.ressource}")
    private String ressourceLogin;

    @Value("${login.keycloak.credentials.secret}")
    private String ressourceCredentials;

    @Value("${global.host}")
    private String keycloakHost;

    @Value("${global.port}")
    private String keycloakPort;

    @Value("${keycloak.realm}")
    private String userRealm;

    public Object loginUser(LoginInput loginInput) throws Exception {

        URIBuilder builder = new URIBuilder();
        builder.setScheme("http").setHost(keycloakHost).setPort(Integer.parseInt(keycloakPort)).setPath("auth/realms/" + userRealm + "/protocol/openid-connect/token");
        URI uri = builder.build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(ressourceLogin, ressourceCredentials);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("username", loginInput.getUsername());
        map.add("password", loginInput.getPassword());
        map.add("grant_type", "password");


        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = restTemplate.postForEntity(uri, request, String.class);
        LOG.info(response.getBody());
        return response;
    }
    @Transactional
    public User registerUser(NewUserInput newUserInput) throws Exception {

        try {
            String newUserId = this.keycloakAdminService.createNewUser(newUserInput);
            User user = new User();
            user.setUsername(newUserInput.getUsername());
            user.setName( (newUserInput.getLastName() != null ? newUserInput.getLastName() : "") + " " + (newUserInput.getFirstName() != null ? newUserInput.getFirstName() : ""));
            user.setUuid(newUserId);
            LOG.info(user.getUsername());
            this.userRepository.save(user);
            return user;

        } catch (Exception e) {
            throw new Exception("Une erreur est survenue lors de la création de l'utilisateur");
        }
    }



    public User findOneBySub(String sub){
        return  this.userRepository.findOneBySub(sub);
    };
}