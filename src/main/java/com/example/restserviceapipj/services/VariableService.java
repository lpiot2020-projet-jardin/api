package com.example.restserviceapipj.services;

import com.example.restserviceapipj.entities.IoT;
import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.entities.Variable;
import com.example.restserviceapipj.repositories.IoTRepository;
import com.example.restserviceapipj.repositories.UserRepository;
import com.example.restserviceapipj.repositories.VariableRepository;
import com.example.restserviceapipj.strategies.IdGenerator;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ServerErrorException;
import java.util.List;

@Service
public class VariableService {

    @Autowired
    private VariableRepository variableRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IoTRepository ioTRepository;

    public Variable findByIdVariable(String idVariable) {
        return this.variableRepository.findOneByIdVariable(idVariable);
    }
    public List<Variable> getAllVariableByIoTIdToken(String idToken, String sub) {
        User user = this.userRepository.findOneBySub(sub);
        List<Variable> variables = this.variableRepository.findAllByIoTIdToken(idToken, user);
        return variables;
    }

    public Variable addVariable(String label, Long iotId)  {
        try {
            Variable variable = new Variable();
            variable.setLabel(label);
            IoT ioT = this.ioTRepository.findById(iotId).orElseThrow();
            variable.setIdVariable(IdGenerator.generateNewId());
            variable.setIot(ioT);
            this.variableRepository.save(variable);
            return variable;
        } catch(Exception e) {
            throw new NotFoundException("Objet connecté non trouvé");
        }
    }

    public Variable updateVariable(String idVariable, String label, Float seuil) {
        try {
            Variable variable = this.variableRepository.findOneByIdVariable(idVariable);
            if(seuil != null) variable.setSeuil(seuil);
            if(label != null) variable.setLabel(label);
            this.variableRepository.save(variable);
            return variable;
        } catch(Exception e) {
            throw new ServerErrorException("Erreur lors de la mise à jour de la variable", 500);
        }
    }

    public boolean deleteVariable(String idVariable) {
        try {
            Variable variable = this.variableRepository.findOneByIdVariable(idVariable);
            this.variableRepository.delete(variable);
            return true;
        } catch (Exception e) {
            throw new ServerErrorException("Erreur lors de la suppression de la variable", 500);
        }
    }

    public Boolean updateVariableValue(String idVariable, String idTokenIot, String tokenIot, Float value) {
        try {
            IoT ioT = this.ioTRepository.findByIdTokenAndToken(idTokenIot, tokenIot);
            if(ioT != null) {
                Variable variable = this.variableRepository.findOneByIdVariable(idVariable);
                variable.setValue(value);
                this.variableRepository.save(variable);
                return true;
            }
            return false;
        } catch (Exception e){
            throw new ServerErrorException("Erreur lors de l'update de la valeur de la variable", 500);
        }
    }
}
