package com.example.restserviceapipj.services;

import com.example.restserviceapipj.entities.Espaces;
import com.example.restserviceapipj.entities.IoT;
import com.example.restserviceapipj.entities.User;
import com.example.restserviceapipj.entities.Variable;
import com.example.restserviceapipj.repositories.EspacesRepository;
import com.example.restserviceapipj.repositories.IoTRepository;
import com.example.restserviceapipj.repositories.UserRepository;
import com.example.restserviceapipj.repositories.VariableRepository;
import com.example.restserviceapipj.strategies.IdGenerator;
import com.example.restserviceapipj.strategies.IoTTokenGenerator;
import org.aspectj.weaver.ast.Var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.eclipse.microprofile.jwt.Claim;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ServerErrorException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class IoTService {

    @Autowired
    private VariableRepository variableRepository;

    @Autowired
    private IoTRepository ioTRepository;

    @Autowired
    private EspacesRepository espacesRepository;

    @Autowired
    private UserRepository userRepository;


    private static final Logger LOG = Logger.getLogger("IotService");


    public List<IoT> getAllIoT(String sub) {
        User user = this.userRepository.findOneBySub(sub);
        List<IoT> iots = this.ioTRepository.findAllByUser(user);
        return iots;
    }

    public IoT addIoT(String name, String description, Long espaceId) throws ClientErrorException {
        try {
            IoT iot = new IoT();
            iot.setName(name);
            iot.setDescription(description);
            Espaces espaces = this.espacesRepository.findById(espaceId).orElseThrow();
            iot.setEspace(espaces);

            iot.setIdToken(IdGenerator.generateNewId());
            iot.setToken(IoTTokenGenerator.generateNewToken());

            this.ioTRepository.save(iot);
            return iot;
        } catch (Exception e) {
            throw new BadRequestException("Espace inexistant");
        }
    }


    public IoT updateIoT(String idToken, String name, String description) throws ClientErrorException {
        try {
            IoT ioT = this.ioTRepository.findByIdToken(idToken);
            if(name != null) ioT.setName(name);
            if(description != null) ioT.setDescription(description);
            this.ioTRepository.save(ioT);
            return ioT;
        } catch(Exception e) {
            throw new ServerErrorException("Erreur lors de la mise à jour de l'IoT", 500);
        }
    }

    public boolean deleteIoT(String idToken) {
        try {
            IoT iot = this.ioTRepository.findByIdToken(idToken);
            for(Variable v : iot.getVariables()) {
                this.variableRepository.delete(v);
            }
            this.ioTRepository.delete(iot);
            return true;
        } catch (Exception e) {
            throw new ServerErrorException("Erreur lors de la suppression de l'IoT", 500);
        }
    }

    public IoT refreshTokenIoT(String idToken) {
        try {
            IoT iot = this.ioTRepository.findByIdToken(idToken);
            iot.setToken(IoTTokenGenerator.generateNewToken());
            this.ioTRepository.save(iot);
            return iot;
        } catch (Exception e) {
            throw new ServerErrorException("Erreur lors du refresh du token de l'IoT", 500);
        }
    }
}