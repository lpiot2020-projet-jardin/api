package com.example.restserviceapipj.services;


import com.example.restserviceapipj.ressources.input.NewUserInput;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.logging.Logger;

@Service
@ApplicationScoped
public class KeycloakAdminService {



    @Value("${global.keycloak.admin.server-url}")
    private String serverUrl;

    @Value("${global.keycloak.admin.realm}")
    private String realm;

    @Value("${global.keycloak.admin.client-id}")
    private String clientId;

    @Value("${global.keycloak.admin.client-secret}")
    private String clientSecret;

    @Value("${global.keycloak.admin.username}")
    private String apiUsername;

    @Value("${global.keycloak.admin.password}")
    private String apiPassword;

    private static Keycloak KEYCLOAK_INSTANCE;
    private static final Logger LOG = Logger.getLogger("KeycloakAdminService");

    /**
     * PostConstruct -> before building the class this method is called.
     * It allows to configure our keycloak instance in order to interact with it
     */
    @PostConstruct
    public void initKeycloakInstance() {
        KEYCLOAK_INSTANCE = KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(realm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .username(apiUsername)
                .password(apiPassword)
                .build();

    }

    /**
     * Creates a new patient and register him in keycloak
     * @param newUserInput The body of the request : the new user
     * @return The id of the new user : it is given after the response
     */
    public String createNewUser(NewUserInput newUserInput) {
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(newUserInput.getUsername());
        user.setFirstName(newUserInput.getFirstName() != null ? newUserInput.getFirstName() : "");
        user.setLastName(newUserInput.getLastName()!= null ? newUserInput.getLastName() : "");
        user.setEmail(newUserInput.getEmail() != null ? newUserInput.getEmail() : "");
        CredentialRepresentation passwordCred = new CredentialRepresentation();
        passwordCred.setTemporary(false);
        passwordCred.setType(CredentialRepresentation.PASSWORD);
        passwordCred.setValue(newUserInput.getPassword());

        user.setCredentials(Collections.singletonList(passwordCred));
        user.setRealmRoles(Collections.singletonList("IOT_USER"));
        UsersResource usersRessource = KEYCLOAK_INSTANCE.realm("Iotuser").users();

        RoleRepresentation roleIot = KEYCLOAK_INSTANCE.realm("Iotuser").roles().get("ROLE_IOT_USER").toRepresentation();

        Response response = usersRessource.create(user);

        if (response.getStatus() == 201) {
            LOG.info("User registered with email : " + user.getEmail());
            usersRessource.get(CreatedResponseUtil.getCreatedId(response)).roles().realmLevel().add(Arrays.asList(roleIot));
            return CreatedResponseUtil.getCreatedId(response);
        } else {
            LOG.info("User registered with email : " + user.getEmail() + " and username " + user.getUsername());
            throw new BadRequestException("Email or username already exists");
        }


    }

    /**
     * Find all users in a realm
     * @param realm The realm we want to find users in
     * @return List of UserRepresentation : the type of user in keycloak
     */
    public List<UserRepresentation> findAllByRealm(String realm) {
        return KEYCLOAK_INSTANCE.realm(realm).users().list(0, 99999);
    }


}
